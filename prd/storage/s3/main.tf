module "logging-bucket" {
  source            = "git::https://gitlab.com/devinitly/terraform/modules/terraform-aws-s3-logging-bucket.git?ref=v1.1.3"
  bucket_name       = var.bucket_name
  region            = var.region
  versioning_status = var.versioning_status
  environment       = var.environment
  canned_acl        = var.canned_acl
  tags              = var.tags
}
