bucket_name       = "devinitly-root-logging"
region            = "us-east-1"
versioning_status = false
canned_acl        = "log-delivery-write"
environment       = "root"
tags              = { Project = "portfolio", Department = "Cloud Engineering" }
