module "standard-vpc" {
  source               = "git::https://gitlab.com/devinitly/terraform/modules/terraform-aws-vpc-standard.git?ref=v1.3.3"
  environment          = var.environment
  nat_instance_key     = var.nat_instance_key
  bastion_instance_key = var.bastion_instance_key
}