data "terraform_remote_state" "network" {
  backend = "s3"
  config = {
    bucket = "devinitly-root-tf-state"
    key    = "prd/networking/vpc/terraform.tfstate"
    region = "us-east-1"
  }
}

module "ecs_task_execution_role" {
  source                           = "git::https://gitlab.com/devinitly/terraform/modules/terraform-aws-iam-role.git?ref=v0.1.4"
  iam_role_name                    = var.ecs_task_execution_role_name
  iam_role_path                    = var.ecs_task_execution_role_path
  iam_role_description             = var.ecs_task_execution_role_description
  iam_role_max_session_duration    = var.ecs_task_execution_role_max_session_duration
  trust_relationship_document_path = var.ecs_task_execution_role_trust_relationship_document_path
  role_policy_document_path        = var.ecs_task_execution_role_policy_document_path
}

module "ecs_task_role" {
  source                           = "git::https://gitlab.com/devinitly/terraform/modules/terraform-aws-iam-role.git?ref=v0.1.4"
  iam_role_name                    = var.ecs_task_role_name
  iam_role_path                    = var.ecs_task_role_path
  iam_role_description             = var.ecs_task_role_description
  iam_role_max_session_duration    = var.ecs_task_role_max_session_duration
  trust_relationship_document_path = var.ecs_task_role_trust_relationship_document_path
  role_policy_document_path        = var.ecs_task_role_policy_document_path
}

resource "aws_security_group" "ecs_security_group" {
  name        = "${var.ecs_cluster_name}-security-group"
  description = "Security group for ${var.ecs_cluster_name} related traffic"
  vpc_id      = data.terraform_remote_state.network.outputs.vpc_id
  tags        = merge({ Name = "${var.ecs_cluster_name}-security-group" }, local.tags)
}

resource "aws_security_group_rule" "ecs_outbound_rule_vpc_cidr" {
  type              = "egress"
  from_port         = 443
  to_port           = 443
  protocol          = "tcp"
  security_group_id = aws_security_group.ecs_security_group.id
  cidr_blocks       = [data.terraform_remote_state.network.outputs.vpc_cidr_block]
}

resource "aws_security_group_rule" "ecs_outbound_rule_endpoint_prefix" {
  type              = "egress"
  from_port         = 443
  to_port           = 443
  protocol          = "tcp"
  security_group_id = aws_security_group.ecs_security_group.id
  prefix_list_ids   = [data.terraform_remote_state.network.outputs.s3_vpc_prefix_list_id]
}

resource "aws_security_group_rule" "ecs_outbound_rule_nat" {
  type                     = "egress"
  from_port                = 443
  to_port                  = 443
  protocol                 = "tcp"
  security_group_id        = aws_security_group.ecs_security_group.id
  source_security_group_id = data.terraform_remote_state.network.outputs.nat_security_group_id
}


module "gitlab-runner-ecs-fargate" {
  source                          = "git::https://gitlab.com/devinitly/terraform/modules/terraform-aws-ecs-fargate.git?ref=v0.1.4"
  vpc_id                          = data.terraform_remote_state.network.outputs.vpc_id
  log_group_prefix                = var.log_group_prefix
  ecs_cluster_name                = var.ecs_cluster_name
  cluster_capacity_providers      = var.cluster_capacity_providers
  ecs_task_definition_family_name = var.ecs_task_definition_family_name
  ecs_task_role_arn               = module.ecs_task_role.iam_role_arn
  ecs_execution_role_arn          = module.ecs_task_execution_role.iam_role_arn
  ecs_task_container_definition   = var.ecs_task_container_definition
  ecs_service_desired_count       = var.ecs_service_desired_count
  ecs_service_name                = var.ecs_service_name
  ecs_service_iam_role            = var.ecs_service_iam_role
  ecs_service_weight              = var.ecs_service_weight
  ecs_service_base                = var.ecs_service_base
  ecs_service_security_groups     = [aws_security_group.ecs_security_group.id]
  ecs_service_subnets = [
    data.terraform_remote_state.network.outputs.aws_subnet_private_1_id,
    data.terraform_remote_state.network.outputs.aws_subnet_private_2_id,
    data.terraform_remote_state.network.outputs.aws_subnet_private_3_id
  ]
}
