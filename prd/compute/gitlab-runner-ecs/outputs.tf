output "gitlab_runner_ecs_security_group_id" {
  value = aws_security_group.ecs_security_group.id
}

output "gitlab_runner_cloudwatch_log_group_arn" {
  value = module.gitlab-runner-ecs-fargate.ecs_cluster_cloud_watch_log_group_arn
}

output "gitlab_runner_ecs_cluster_id" {
  value = module.gitlab-runner-ecs-fargate.ecs_cluster_id
}

output "gitlab_runner_ecs_cluster_arn" {
  value = module.gitlab-runner-ecs-fargate.ecs_cluster_arn
}

output "gitlab_runner_ecs_service_id" {
  value = module.gitlab-runner-ecs-fargate.ecs_service_id
}

output "gitlab_runner_ecs_service_name" {
  value = module.gitlab-runner-ecs-fargate.ecs_service_name
}

output "gitlab_runner_ecs_task_definition_arn" {
  value = module.gitlab-runner-ecs-fargate.ecs_task_definition_arn
}

output "gitlab_runner_ecs_task_defintion_family" {
  value = module.gitlab-runner-ecs-fargate.ecs_task_definition_family
}

output "gitlab_runner_ecs_task_defintion_revision" {
  value = module.gitlab-runner-ecs-fargate.ecs_task_definition_revision
}

