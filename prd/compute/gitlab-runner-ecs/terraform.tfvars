environment                                              = "ROOT"
ecs_task_execution_role_name                             = "gitlab-ci-task-execution-role"
ecs_task_execution_role_trust_relationship_document_path = "./policy-files/ecs-task-execution-trust.json"
ecs_task_execution_role_policy_document_path             = "./policy-files/ecs-task-execution-role.json"
ecs_task_execution_role_path                             = "/gitlabci/"
ecs_task_execution_role_description                      = "Gitlab CI execution role for ecs tasks"
ecs_task_execution_role_max_session_duration             = "3600"
ecs_task_execution_policy_description                    = "Gitlab CI ecs task role to create necessary aws resources for deployment"

ecs_task_role_name                             = "gitlab-ci-task-role"
ecs_task_role_trust_relationship_document_path = "./policy-files/ecs-task-trust.json"
ecs_task_role_policy_document_path             = "./policy-files/ecs-task-role.json"
ecs_task_role_description                      = "Gitlab ci task role to assume admin access for infrastructure pipline"
ecs_task_role_max_session_duration             = "3600"

// ECS DEFINITIONS
log_group_prefix                = "/ecs/"
ecs_cluster_name                = "gitlab-ci-ecs-cluster"
cluster_capacity_providers      = ["FARGATE"]
ecs_task_definition_family_name = "gitlab-ci-runner"
ecs_task_cpu                    = "512"
ecs_task_memory                 = "1024"
ecs_task_container_definition = [
  {
    image = "134905014910.dkr.ecr.us-east-1.amazonaws.com/gitlab-runner-terraform:latest"
    name  = "gitlab-ci"
    logConfiguration = {
      logdriver = "awslogs"
      options = {
        "awslogs-group"         = "/ecs/gitlab-ci-runner"
        "awslogs-region"        = "us-east-1"
        "awslogs-stream-prefix" = "ecs"
      }
    }
    portMappings = [
      {
        hostPort      = 80
        protocol      = "tcp"
        containerPort = 80
      },
      {
        hostPort      = 443
        protocol      = "tcp"
        containerPort = 443
    }]
    command = ["/start.sh"]
    secrets = [{
      name      = "TOKEN"
      valueFrom = "arn:aws:secretsmanager:us-east-1:134905014910:secret:TOKEN-DBr8lF"
    }]
    workingDirectory  = "/home/"
    memoryReservation = 700
    essential         = true
  }
]

ecs_service_name          = "gitlab-ci-service"
ecs_service_desired_count = 1
ecs_service_weight        = 1
ecs_service_base          = 0
