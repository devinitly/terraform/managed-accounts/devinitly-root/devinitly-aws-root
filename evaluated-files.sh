#!/usr/bin/env bash

files=$(git diff-tree --no-commit-id --name-only -r "$CI_COMMIT_SHA")
for file in $files
do
  parsed_file=${file##*/}
  if [[ "$parsed_file" =~ .(tf|tfvars)$ ]]; then
    parsed_path=${file%/*}
    export TF_PATH="$parsed_path"
    cd "$parsed_path" || exit
    terragrunt plan -out terraform.plan
    echo "$TF_PATH"
  fi
done